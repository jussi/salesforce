import { z } from 'zod'

export const SalesforceAccountInput = z.object({
  Id: z.string(),
  Name: z.string(),
  CRM_External_ID__c: z.string().nullable(),
  SAPCustomerNumber__c: z.string().nullable(),
  CGT_External_ID__c: z.string().nullable(),
  CGT_Customer_Language__c: z.string().nullable(),
  BillingStreet: z.string().nullable(),
  BillingCity: z.string().nullable(),
  BillingState: z.string().nullable(),
  BillingPostalCode: z.string().nullable(),
  BillingCountry: z.string().nullable(),
  BillingStateCode: z.string().nullable(),
  ShippingStreet: z.string().nullable(),
  ShippingCity: z.string().nullable(),
  ShippingState: z.string().nullable(),
  ShippingPostalCode: z.string().nullable(),
  ShippingCountry: z.string().nullable(),
  ShippingStateCode: z.string().nullable(),
})

export const SalesforceStreetAddressSchema = z.object({
  street: z.string().nullable(),
  city: z.string().nullable(),
  state: z.string().nullable(),
  postalCode: z.string().nullable(),
  country: z.string().nullable(),
  stateCode: z.string().nullable(),
})

export const SalesforceAccount = z.object({
  id: z.string(),
  name: z.string(),
  SAPCustomerNumber: z.string().nullable(),
  billingAddress: SalesforceStreetAddressSchema,
  shippingAddress: SalesforceStreetAddressSchema,
})

// Transforms the salesforce domain logic into something human readable
export const TransformInputAccount = SalesforceAccountInput.transform((value) =>
  SalesforceAccount.parse({
    id: value.Id,
    name: value.Name,
    SAPCustomerNumber: value.SAPCustomerNumber__c,
    billingAddress: {
      street: value.BillingStreet,
      city: value.BillingCity,
      state: value.BillingState,
      postalCode: value.BillingPostalCode,
      country: value.BillingCountry,
      stateCode: value.BillingStateCode,
    },
    shippingAddress: {
      street: value.ShippingStreet,
      city: value.ShippingCity,
      state: value.ShippingState,
      postalCode: value.ShippingPostalCode,
      country: value.ShippingCountry,
      stateCode: value.ShippingStateCode,
    },
  }),
)

// Input type
export type InputAccount = z.output<typeof SalesforceAccountInput>

// Output type
export type SalesforceAccount = z.output<typeof SalesforceAccount>

export const SalesforceContactInput = z.object({
  Id: z.string(),
  AccountId: z.string(),
  Name: z.string().optional(),
  Email: z.string().email(),
  Phone: z.string().optional(),
  FirstName: z.string().optional(),
  LastName: z.string().optional(),
})

export const SalesforceContact = z.object({
  id: z.string(),
  accountId: z.string(),
  name: z.string().optional(),
  email: z.string().email(),
  phone: z.string().optional(),
  firstName: z.string().optional(),
  lastName: z.string().optional(),
  communicationLanguageCode: z.string().optional(),
  accountType: z.string().optional(),
})

// Input type
export type InputContact = z.output<typeof SalesforceContactInput>

// Output type
export type SalesforceContact = z.output<typeof SalesforceContact>

export const TransformInputContact = SalesforceContactInput.transform((value) =>
  SalesforceContact.parse({
    id: value.Id,
    accountId: value.AccountId,
    name: value.Name,
    email: value.Email,
    phone: value.Phone,
    firstName: value.FirstName,
    lastName: value.LastName,
  }),
)
