import { RequestOptions, RESTDataSource } from 'apollo-datasource-rest'
import { URLSearchParamsInit } from 'apollo-server-env'

import { env } from '@utils/env'
import {
  ACCOUNT_GROUPS,
  ACCOUNT_RECORD_TYPE_ID,
  ACCOUNT_SEARCH_TERMS,
  AccountQueryOptions,
  AccountQueryResult,
  buildSalesforceQuery,
  Clause,
  ContactQueryOptions,
  ContactQueryResult,
  QueryResult,
} from '@datasources/salesforce/QueryBuilder'
import {
  SalesforceAccount,
  SalesforceAccountInput,
  SalesforceContact,
  SalesforceContactInput,
  TransformInputAccount,
  TransformInputContact,
} from '@datasources/salesforce/schemas'
import logger from '@utils/logger'
import { getSalesforceApiToken, SalesForceOAuthToken } from '@utils/SalesforceApi'

const ACCOUNT_QUERY_FIELDS = Object.keys(SalesforceAccountInput.shape)
const CONTACT_QUERY_FIELDS = Object.keys(SalesforceContactInput.shape)

export class SalesforceDataSource extends RESTDataSource {
  static API_VERSION = '54.0'
  private static token: string | null

  constructor() {
    super()
    this.baseURL = `${env.SF_API_URL}/services/data/v${SalesforceDataSource.API_VERSION}`
  }

  async getAccounts(accountQueryOptions: AccountQueryOptions): Promise<ReadonlyArray<SalesforceAccount>> {
    const selectedTerms = Object.keys(accountQueryOptions)

    const searchTerms: Clause[] = selectedTerms.map((term): Clause => {
      const queryOperationClause: Partial<Clause> = ACCOUNT_SEARCH_TERMS[term]

      if (!queryOperationClause) {
        throw new Error(`Unknown search term: ${term}`)
      }

      const searchTermValue = accountQueryOptions[term]

      if (!searchTermValue) {
        throw new Error(`Search term ${term} is not defined`)
      }

      return <Clause>{
        ...queryOperationClause,
        value: searchTermValue,
      }
    })

    if (searchTerms.length === 0) {
      throw new Error(`SAP ID, VAT ID or company name must be provided`)
    }

    const accountQueryResult = await this.getWithRetry<AccountQueryResult>('/query/', {
      q: buildSalesforceQuery({
        tableName: 'Account',
        fields: ACCOUNT_QUERY_FIELDS,
        where: {
          operation: 'AND',
          value: [
            {
              operation: 'OR',
              value: [
                {
                  field: 'CGT_Account_Group__c',
                  operation: 'IN',
                  value: ACCOUNT_GROUPS,
                },
                {
                  field: 'RecordTypeId',
                  operation: '=',
                  value: ACCOUNT_RECORD_TYPE_ID,
                },
              ],
            },
            {
              operation: 'OR',
              value: searchTerms,
            },
          ],
        },
      }),
    })

    const accountRecords: ReadonlyArray<unknown> = await this.respondWithRecords<unknown>(accountQueryResult)

    return accountRecords.map((value) => TransformInputAccount.parse(value))
  }

  async getContacts(contactQueryOptions: ContactQueryOptions): Promise<ReadonlyArray<SalesforceContact>> {
    const { accountId } = contactQueryOptions
    const contactQueryResult = await this.getWithRetry<ContactQueryResult>('/query/', {
      q: buildSalesforceQuery({
        tableName: 'Contact',
        fields: CONTACT_QUERY_FIELDS,
        where: {
          field: 'AccountId',
          operation: '=',
          value: accountId,
        },
      }),
    })

    const contactRecords: ReadonlyArray<unknown> = await this.respondWithRecords<unknown>(contactQueryResult)
    return contactRecords.map((value) => TransformInputContact.parse(value))
  }

  // Sometimes the API responses contain a resource path to fetch
  // more data.
  private async nextRecords<T>(nextResourceUrl: string): Promise<ReadonlyArray<T>> {
    logger.debug({ input: nextResourceUrl })
    const results = await this.getWithRetry<QueryResult<T>>(`${env.SF_API_URL}/${nextResourceUrl}`, {})

    const { done, records } = results

    return done ? records : [...records, ...(await this.nextRecords<T>(results.nextRecordsUrl))]
  }

  override async willSendRequest(request: RequestOptions): Promise<void> {
    request.headers.set('Authorization', SalesforceDataSource.token || (await SalesforceDataSource.getToken()))
  }

  private static async getToken(): Promise<string> {
    if (this.token) {
      return this.token
    }
    const { access_token }: SalesForceOAuthToken = await getSalesforceApiToken()

    this.token = `Bearer ${access_token}`
    return this.token
  }

  private async respondWithRecords<T>(queryResult: QueryResult<T>): Promise<ReadonlyArray<T>> {
    const { records } = queryResult

    if (queryResult.done) {
      logger.debug({ recordsFound: records.length })
      return records
    }

    // We aren't done and need to fetch the rest of the records from
    // the nextRecordsUrl location cursor.
    const nextResults = await this.nextRecords<T>(queryResult.nextRecordsUrl)

    const totalRecords = [...records, ...nextResults]
    logger.debug({ recordsFound: totalRecords.length })
    return totalRecords
  }

  private async getWithRetry<T>(path: string, options: URLSearchParamsInit, attempt = 0): Promise<T> {
    try {
      logger.debug({ method: 'GET', path, options, attempt })
      return await this.get(path, options)
    } catch (e) {
      // In case of an error, let's assume that the token has expired and try to get a new one
      SalesforceDataSource.token = null
      if (attempt <= 2) {
        logger.error({ error: e, attempt })

        return this.getWithRetry(path, options, attempt + 1)
      } else {
        throw e
      }
    }
  }
}
