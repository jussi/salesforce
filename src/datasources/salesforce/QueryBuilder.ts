import { isArray } from 'lodash'

import { InputAccount, InputContact } from '@datasources/salesforce/schemas'
import logger from '@utils/logger'
import { AtLeastOne } from '@utils/types'

export const ACCOUNT_GROUPS = ['C006', 'C018']
export const ACCOUNT_RECORD_TYPE_ID = '0121o000000gX0xAAE'
export const ACCOUNT_SEARCH_TERMS: AccountSearchTerms = {
  sapId: {
    field: 'SAP_Number__c',
    operation: 'LIKE',
  },
  companyName: {
    field: 'Name',
    operation: 'LIKE',
  },
  vatId: {
    field: 'CGT_VAT_Registration_Number__c',
    operation: 'LIKE',
  },
}

export type AccountSearchTerms = {
  sapId: Partial<Clause>
  companyName: Partial<Clause>
  vatId: Partial<Clause>
}

export type QueryResult<T> =
  | { records: ReadonlyArray<T>; totalSize: number; done: true }
  | { records: ReadonlyArray<T>; totalSize: number; done: false; nextRecordsUrl: string }

export type AccountQueryResult = QueryResult<InputAccount>
export type ContactQueryResult = QueryResult<InputContact>

export type ContactQueryOptions = {
  accountId: string
}

export type AccountQueryOptions = AtLeastOne<{
  [K in keyof AccountSearchTerms]: string
}>

export type Clause =
  | {
      field: string
      operation: '=' | 'LIKE'
      value: string
    }
  | {
      field: string
      operation: 'IN'
      value: string[]
    }
  | { operation: 'AND' | 'OR'; value: Clause[] }

export type Query = {
  tableName: string
  fields: ReadonlyArray<string>
  where: Clause
}

export const addQuotes = (value: string): string => `'${value}'`

export const parseOperation = (op: Clause): string => {
  const { operation } = op

  switch (operation) {
    case 'IN': {
      const values = isArray(op.value) ? op.value : [op.value]
      return `${op.field} IN (${values.map(addQuotes).join(',')})`
    }
    case 'LIKE':
      return `${op.field} LIKE '${op.value}'`
    case '=':
      return `${op.field} = '${op.value}'`
    case 'OR':
    case 'AND':
      return `(${op.value.map(parseOperation).join(` ${operation} `)})`
  }

  throw new Error(`Unknown operation: ${operation}`)
}

export const whereClause = (operation?: Clause): string => {
  if (!operation) {
    return ''
  }
  return `WHERE ${parseOperation(operation)}`
}

export const buildSalesforceQuery = (query: Query): string => {
  const { tableName, fields, where } = query
  const builtQuery = `SELECT ${fields.join(',')} FROM ${tableName} ${whereClause(where)}`
  logger.debug(`Performing Salesforce query: ${builtQuery}`)
  return builtQuery
}
