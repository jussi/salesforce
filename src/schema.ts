import 'reflect-metadata'

import { GraphQLSchema } from 'graphql'
import { buildSchemaSync } from 'type-graphql'

import { HelloQueryResolver } from '@resolvers/HelloResolver'

const schema: GraphQLSchema = buildSchemaSync({
  dateScalarMode: 'isoDate',
  resolvers: [HelloQueryResolver],
})

export default schema
