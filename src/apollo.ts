import { ApolloServer } from 'apollo-server'

import schema from './schema'
import { env } from './utils/env'
import { SalesforceDataSource } from './datasources/salesforce'

export type ExampleApolloContext = {
  dataSources: {
    salesforce: SalesforceDataSource
  }
}

export const apolloServer = new ApolloServer({
  schema: schema,
  debug: env.NODE_ENV !== 'production',
  cors: process.env['NODE_ENV'] === 'development' ? { origin: env.ALLOWED_CORS_ORIGINS, credentials: true } : {},
  dataSources: () => ({
    salesforce: new SalesforceDataSource(),
  }),
})
