import { Field, ID, ObjectType } from 'type-graphql'

@ObjectType('AddressType')
export class AddressType {
  @Field(() => String, { nullable: true })
  street: string

  @Field(() => String, { nullable: true })
  city: string

  @Field(() => String, { nullable: true })
  state: string

  @Field(() => String, { nullable: true })
  postalCode: string

  @Field(() => String, { nullable: true })
  country: string

  @Field(() => String, { nullable: true })
  stateCode: string
}

@ObjectType('AccountType')
export class AccountType {
  @Field(() => ID, { nullable: false })
  id: string

  @Field(() => String, { nullable: true })
  name: string

  @Field(() => String, { nullable: true })
  SAPCustomerNumber: string

  @Field(() => AddressType, { nullable: true })
  billing: AddressType

  @Field(() => AddressType, { nullable: true })
  shipping: AddressType
}
