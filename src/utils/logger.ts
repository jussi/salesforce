import { ISettingsParam, Logger } from 'tslog'

import { env } from './env'

const options: ISettingsParam = {
  name: 'app',
  type: env.NODE_ENV === 'development' ? 'pretty' : 'json',
  minLevel: env.NODE_ENV === 'development' ? 'silly' : 'info',
}

const logger: Logger = new Logger(options)

export default logger
