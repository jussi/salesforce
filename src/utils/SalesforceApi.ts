export type SalesForceOAuthToken = {
  access_token: string
  scope: string
  instance_url: string
  id: string
  token_type: string
}

export const getSalesforceApiToken = async (): Promise<SalesForceOAuthToken> => {
  // Code redacted for brevity
  return {
    id: 'foobar',
    access_token: 'foobar',
    scope: 'foobar',
    instance_url: 'foobar',
    token_type: 'foobar',
  }
}
