import { parseEnvironmentVariables } from '@absxn/process-env-parser'
import { URL } from 'url'

const parser = {
  url: (s: string | null): string => {
    if (!s) {
      throw new Error('URL required')
    }
    // throws an error if URL could not be parsed
    new URL(s).toString()

    return s
  },
}

const parseRequiredEnvs = () => {
  console.log('Parsing required environment variables')
  const result = parseEnvironmentVariables({
    NODE_ENV: { default: 'development' },
    HTTP_PORT: { default: 3333 },
    SF_API_URL: { default: 'https://example.org', parser: parser.url },
    ALLOWED_CORS_ORIGINS: { default: 'https://studio.apollographql.com' },
  })

  if (result.success) {
    return result.env
  }

  console.table(result.envPrintable)

  process.exit(1)
}

export const env = parseRequiredEnvs()
