export type ArrayElement<ArrayType extends readonly unknown[]> = ArrayType extends readonly (infer ElementType)[]
  ? ElementType
  : never

// eslint-disable-next-line
export type NonNullableProps<T extends unknown, K extends keyof T> = {
  [TK in keyof T]: TK extends K ? NonNullable<T[TK]> : T[TK]
}

export type NoUndefinedField<T> = {
  [P in keyof T]-?: NoUndefinedField<NonNullable<T[P]>>
}

export type AtLeastOne<T, U = { [K in keyof T]: Pick<T, K> }> = Partial<T> & U[keyof U]
