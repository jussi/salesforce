import { Arg, Ctx, Query, Resolver } from 'type-graphql'

import { ExampleApolloContext } from '@backend/apollo'
import { SalesforceAccount } from '@datasources/salesforce/schemas'
import logger from '@utils/logger'
import { AccountType } from '@entities/Accounts'

@Resolver(() => AccountType)
export class HelloQueryResolver {
  @Query(() => [AccountType])
  async searchAccount(
    @Ctx() context: ExampleApolloContext,
    @Arg('companyName', () => String, { nullable: true }) companyName: string,
  ): Promise<ReadonlyArray<SalesforceAccount>> {
    try {
      return context.dataSources.salesforce.getAccounts({ companyName })
    } catch (error) {
      logger.error(error)
      throw error
    }
  }
}
