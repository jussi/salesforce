import http from 'http'

import { ServerInfo } from 'apollo-server'

import logger from '@utils/logger'
import { env } from '@utils/env'

import { apolloServer } from './apollo'

const serverStarted = ({ server, url }: ServerInfo): http.Server => {
  logger.info({ message: `🚀 Server ready at ${url}` })
  logger.info({ message: `Try your health check at: ${url}.well-known/apollo/server-health` })
  return server
}

export default apolloServer.listen({ port: env.HTTP_PORT }).then(serverStarted).catch(console.error)
