import test from 'ava'

import { buildSalesforceQuery, Clause, parseOperation } from '@datasources/salesforce/QueryBuilder'

test('IN clause is parsed properly', async (t) => {
  const inClause: Clause = {
    field: 'id',
    operation: 'IN',
    value: ['1', '2', '3'],
  }

  t.is(`id IN ('1','2','3')`, parseOperation(inClause))
})

test('LIKE clause is parsed properly', async (t) => {
  const inClause: Clause = {
    field: 'id',
    operation: 'LIKE',
    value: 'foo%',
  }

  t.is(`id LIKE 'foo%'`, parseOperation(inClause))
})

test('Equals clause is parsed properly', async (t) => {
  const inClause: Clause = {
    field: 'id',
    operation: '=',
    value: 'foo',
  }

  t.is(`id = 'foo'`, parseOperation(inClause))
})

test('Unknown clause throws an error', async (t) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const inClause: any = {
    field: 'id',
    operation: 'xyz',
    value: ['foo'],
  }
  const error = t.throws(() => parseOperation(inClause))
  t.is(error?.message, 'Unknown operation: xyz')
})

test('Query builder handles a basic IN search', async (t) => {
  const query = `SELECT id,name FROM foo WHERE id IN ('1','2','3')`
  t.is(
    query,
    buildSalesforceQuery({
      fields: ['id', 'name'],
      tableName: 'foo',
      where: {
        field: 'id',
        operation: 'IN',
        value: ['1', '2', '3'],
      },
    }),
  )
})

test('Query builder handles a nested search', async (t) => {
  const expected = `SELECT id,name FROM foo WHERE ((group IN ('C006','C018') OR record = '0121o000000gX0xAAE') AND (first LIKE 'foo' AND second = 'bar' AND second IN ('bar')))`

  t.is(
    expected,
    buildSalesforceQuery({
      fields: ['id', 'name'],
      tableName: 'foo',
      where: {
        operation: 'AND',
        value: [
          {
            operation: 'OR',
            value: [
              {
                field: 'group',
                operation: 'IN',
                value: ['C006', 'C018'],
              },
              {
                field: 'record',
                operation: '=',
                value: '0121o000000gX0xAAE',
              },
            ],
          },
          {
            operation: 'AND',
            value: [
              { field: 'first', operation: 'LIKE', value: 'foo' },
              { field: 'second', operation: '=', value: 'bar' },
              { field: 'second', operation: 'IN', value: ['bar'] },
            ],
          },
        ],
      },
    }),
  )
})
